#!/bin/bash 
#$1 matrice lineaire; $2 nombre de lignes; $3 nombre de colonnes

celluleEnFeu(){

    #echo true si la celulle est en feu
    #$1 matrice linéaire; $2 indice i; $3 indice j; $4 nombre de ligne.
    if [ `./extraireIndice.sh "$1" "$2 " "$3" "$4" ` -eq 2 ]
    then
        echo 1
    else
        echo 0
    fi 
}

voisinEnFeu(){
#affiche 1 si un voisin est en feu (2)
#$1 matrice linéaire; $2 indice de la ligne; $3 indice colonn; $4 nombre de lignes; $5 nombre de colonnes

if [ "$2" -gt 1 ] && [ "$3" -gt 1 ] && [ "$2" -lt "$4" ] && [ "$3" -lt "$5" ]  # On ne se trouve pas sur un bord
then
    let "i=$2 + 1"
    let "j=$3 "
    if [ `celluleEnFeu "$1" "$i" "$j" "$2"` -eq 1 ]
        then 
            echo 1
        else
        let "i=$2 + 1"
        let "j=$3 + 1"
        if [ `celluleEnFeu "$1" "$i" "$j" "$2"` -eq 1 ] 
            then 
                echo 1
            else


            let "i=$2 + 1"
            let "j=$3 - 1"
            if [ `celluleEnFeu "$1" "$i" "$j" "$2"` -eq 1 ]
                then 
                    echo 1
                else


                let "i=$2 - 1"
                let "j=$3"
                if [ `celluleEnFeu "$1" "$i" "$j" "$2"` -eq 1 ]
                    then 
                        echo 1
                    else


                    let "i=$2 - 1"
                    let "j=$3 + 1"
                    if [ `celluleEnFeu "$1" "$i" "$j" "$2"` -eq 1 ]
                        then
                            echo 1
                        else 

                        let "i=$2 - 1"
                        let "j=$3 - 1"
                        if [ `celluleEnFeu "$1" "$i" "$j" "$2"` -eq 1 ]
                            then 
                                echo 1 
                            else

                            let "i=$2"
                            let "j=$3 - 1"
                            if [ `celluleEnFeu "$1" "$i" "$j" "$2"` -eq 1 ]
                                then 
                                    echo 1
                                else

                                let "i=$2"
                                let "j=$3 + 1"
                                if [ `celluleEnFeu "$1" "$i" "$j" "$2"` -eq 1 ]
                                    then 
                                        echo 1
                                    else
                                        echo 0
                                fi
                            fi
                        fi
                    fi
                fi
            fi
        fi
    fi

elif [ "$2" -eq 1 ]; then #On se trouve au bord sup
    let "i=$2 + 1"
    let "j=$3"
    if [ `celluleEnFeu "$1" "$i" "$j" "$2"` -eq 1 ]; then #On vérifie en bas
            echo 1
    elif [ "$3" -eq 1 ]; then #On se trouve sur le bord gauche

        let "i=$2"
        let "j=$3 + 1"
        if [ `celluleEnFeu "$1" "$i" "$j" "$2"` -eq 1 ]; then #On vérifie à droite
            echo 1
        else
            let "i=$2 + 1"
            let "j=$3 + 1"
            if [ `celluleEnFeu "$1" "$i" "$j" "$2"` -eq 1 ]; then #On vérifie en bas  à droite
                echo 1
            else
                echo 0
            fi
        fi
    elif [ "$3" -eq "$5" ]; then #On se trouve sur le bord droit 
        let "i=$2"
        let "j=$3 -1"
        if [ `celluleEnFeu "$1" "$i" "$j" "$2"` -eq 1 ]; then #On vérifie à gauche
            echo 1
        else
            let "i=$2 + 1"
            let "j=$3 - 1"
            if [ `celluleEnFeu "$1" "$i" "$j" "$2"` -eq 1 ]; then #On vérifie en bas à gauche
                echo 1
            else 
                echo 0
            fi
        fi
    else #On se trouve vers le milieu
        let "i=$2"
        let "j=$3 + 1"
        if [ `celluleEnFeu "$1" "$i" "$j" "$2"` -eq 1 ] #On vérifie à droite
            then
                echo 1
            else
            let "i=$2 + 1"
            let "j=$3 + 1"
            if [ `celluleEnFeu "$1" "$i" "$j" "$2"` -eq 1 ] #On vérifie en bas  à droite
                then 
                    echo 1
                else 
                let "i=$2"
                let "j=$3 -1"
                if [ `celluleEnFeu "$1" "$i" "$j" "$2"` -eq 1 ] #On vérifie à gauche
                    then
                        echo 1
                    else
                    let "i=$2 + 1"
                    let "j=$3 - 1"
                    if [ `celluleEnFeu "$1" "$i" "$j" "$2"` -eq 1 ] #On vérifie en bas à gauche
                        then 
                            echo 1
                        else 
                            echo 0
                    fi
                fi
            fi
        fi
    fi
elif [ "$2" -eq "$4" ]; then  #On se trouve sur le bord inf
    let "i=$2 - 1"
    let "j=$3"
    if [ `celluleEnFeu "$1" "$i" "$j" "$2"` -eq 1 ]; then #On vérifie en  haut
        echo 1
    elif [ "$3" -eq 1 ]; then #On se trouve sur le bord gauche
        let "i=$2"
        let "j=$3 + 1"
        if [ `celluleEnFeu "$1" "$i" "$j" "$2"` -eq 1 ]; then #On vérifie à droite
            echo 1
        else
            let "i=$2 - 1"
            let "j=$3 + 1"
            if [ `celluleEnFeu "$1" "$i" "$j" "$2"` -eq 1 ]; then #On vérifie en haut  à droite
                echo 1
            else
                echo 0
            fi
        fi
    
    elif [ "$3" -eq "$5" ]; then #On se trouve sur le bord droit  
        let "i=$2"
        let "j=$3 -1"
        if [ `celluleEnFeu "$1" "$i" "$j" "$2"` -eq 1 ]; then #On vérifie à gauche
            echo 1
        else
            let "i=$2 - 1"
            let "j=$3 - 1"
            if [ `celluleEnFeu "$1" "$i" "$j" "$2"` -eq 1 ]; then #On vérifie en haut à gauche
                echo 1
            else 
                echo 0
            fi
        fi
    else #On se trouve vers le milieu
        let "i=$2"
        let "j=$3 + 1"
        if [ `celluleEnFeu "$1" "$i" "$j" "$2"` -eq 1 ]; then #On vérifie à droite
            echo 1
        else
            let "i=$2 - 1"
            let "j=$3 + 1"
            if [ `celluleEnFeu "$1" "$i" "$j" "$2"` -eq 1 ]; then #On vérifie en haut  à droite
                echo 1
            else 
                let "i=$2"
                let "j=$3 -1"
                if [ `celluleEnFeu "$1" "$i" "$j" "$2"` -eq 1 ]; then #On vérifie à gauche
                    echo 1
                else
                    let "i=$2 - 1"
                    let "j=$3 - 1"
                    if [ `celluleEnFeu "$1" "$i" "$j" "$2"` -eq 1 ]; then #On vérifie en haut à gauche
                        echo 1
                    else 
                        echo 0
                    fi
                fi
            fi
        fi
    fi

 elif [ "$3" -eq 1 ] #On se trouve sur le bord gacuhe vers milieu
    then
    let "i=$2 + 1"
    let "j=$3 "
    if [ `celluleEnFeu "$1" "$i" "$j" "$2"` -eq 1 ]
        then 
            echo 1
        else
        let "i=$2 + 1"
        let "j=$3 + 1"
        if [ `celluleEnFeu "$1" "$i" "$j" "$2"` -eq 1 ] 
            then 
                echo 1
            else


            let "i=$2 - 1"
            let "j=$3"
            if [ `celluleEnFeu "$1" "$i" "$j" "$2"` -eq 1 ]
                then 
                    echo 1
                else


                let "i=$2 - 1"
                let "j=$3 + 1"
                if [ `celluleEnFeu "$1" "$i" "$j" "$2"` -eq 1 ]
                    then
                        echo 1
                    else 


                    let "i=$2"
                    let "j=$3 + 1"
                    if [ `celluleEnFeu "$1" "$i" "$j" "$2"` -eq 1 ]
                        then 
                            echo 1
                        else
                            echo 0
                    fi
                fi
            fi
        fi
    fi

elif [ "$3" -eq "$5" ] #On se trouve sur le bord droit vers milieu
then
    let "i=$2 + 1"
    let "j=$3 "
    if [ `celluleEnFeu "$1" "$i" "$j" "$2"` -eq 1 ]
        then 
            echo 1
        else
   

        let "i=$2 + 1"
        let "j=$3 - 1"
        if [ `celluleEnFeu "$1" "$i" "$j" "$2"` -eq 1 ]
            then 
                echo 1
            else


            let "i=$2 - 1"
            let "j=$3"
            if [ `celluleEnFeu "$1" "$i" "$j" "$2"` -eq 1 ]
                then 
                    echo 1
                else
                let "i=$2 - 1"
                let "j=$3 - 1"
                if [ `celluleEnFeu "$1" "$i" "$j" "$2"` -eq 1 ]
                    then 
                        echo 1 
                    else

                    let "i=$2"
                    let "j=$3 - 1"
                    if [ `celluleEnFeu "$1" "$i" "$j" "$2"` -eq 1 ]
                        then 
                            echo 1
                        else
                            echo 0
                    fi
                fi
            fi
        fi
    fi
fi
}

