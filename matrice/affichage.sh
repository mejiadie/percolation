#!/bin/bash

i=0
t='`wc -c matrice.txt | cut -dsep -f3`

for i in `seq 1 t-1`
do
	chiffre=`cut -c$i matrice.txt`
        case $chiffre in
        1) echo -e "\033[32m +  \033[0m" ;;
        0) echo -e "\033[1;30m +  \033[0m" ;;
        2) echo -e "\033[1;31m +  \033[0m" ;;
        3) echo -e  "\033[33m +  \033[0m" ;;
esac
done
